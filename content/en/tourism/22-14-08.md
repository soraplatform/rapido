Sunday, August 14, 2022
Philippine International Dive Expo (PHIDEX) 2022


The Philippines is now ready to welcome scuba divers and dive enthusiasts from all over the globe. 

The Department of Tourism will once again hold the Philippine International Dive Expo (PHIDEX) 2022 on August 19 to 21, 2022, at the SMX Convention Center in Pasay City, Metro Manila. The three-day hybrid event will gather local and international dive industry experts, dive tour operators, and partner dive businesses to share their ideas and experiences. Exhibition booths will also showcase dive resorts and centers in different destinations, as well as top-rated dive gear and equipment. #divephilippines #diveshow #scubadivers

See you at PHIDEX 2022! Register now at https://phidex.asia/. 

P.S. No quarantine as long as you’re vaccinated and it’s visa free for Americans,  Canadians, and other foreign national travelers visiting the Philippines for 30 days or less  — https://lnkd.in/gGQQBfHH.

Wednesday, August 10, 2022
The 10th World Innovation Series Philippines


To enable financial inclusion in the region and provide a common platform for the burgeoning FSI community, Tradepass is hosting 10th World Innovation Series (WFIS) for the first time in the Philippines on 16 – 17 August 2022. The event will attract over 600 (in-person & virtual) technology and business heads from the leading Banks, Insurance & Micro-Finance institutions across the country.
Do not miss out on the largest fintech summit in Philippines at Sofitel Philippine Plaza Manila. 

More than 400 technology and business leaders from 150+ leading Philippine banks, insurance companies, and microfinance institutions will attend #WFISPhilippines for two days of special networking opportunities.

Register here:   https://philippines.worldfis.com/delegates/register.html

#WFISPhilippines #WFIS2022 #fintech #Digital #physicalEvent #FSI #BFSI #Retail #CoreBanking #Payments #saas #Conference #Technology #IT #IoT #AI #Speakers #Experts #tradepass #innovation #banking #future #newera #cloud #financial #insurance #media #digitalphilippines


