---
title: "Earthquake Rocks Northern Luzon"
banner: "/photos/quake.jpg"
# heading: "A New Science Based on Metaphysics and Dialectics"
subtitle: "A magnitude 7.0 earhquake rocked Abra on the morning of July 27."
linktext: "Read More"
link: "/local/22-07-27a"

mini1: "The Nature of the Soul"
heading1: "Everything is a wave that manifests as a particle"
sub1: "The analog universe streams in real-definition to create reality"
mini2: "The Soul of the Physical Universe"
heading2: "Material Superphysics"
sub2: "Based on the five layers of the ancient Greek, Hindus, and Chinese"

mini3: "The Soul of Living Entities"
heading3: "Medical Superphysics"
sub3: "Based on five medical paradigms"

mini4: "The Soul of Society"
heading4: "Social Superphysics"
sub4: "Fellow-Feeling Instead of Selfish-Interest"


tricon1:
  a:
    - icon: "/icons/tao.png"
      title: "Metaphysics"
      subtitle: "0-50% Replicable: Paradoxical"
  b:
    - icon: "/icons/sp.png"
      title: "Superphysics"
      subtitle: "51-99% Replicable: Subjective"
  c:
    - icon: "/icons/atom.png"
      title: "Physics"
      subtitle: "100% Replicable: Objective"
   
triconsup:
  a:
    - url: "/superphysics/principles/matrix"
      icon: "<svg xmlns='http://www.w3.org/2000/svg' fill='coral' height='80' viewBox='0 0 640 512' fill='coral' height='80'><path d='M624 416H381.54c-.74 19.81-14.71 32-32.74 32H288c-18.69 0-33.02-17.47-32.77-32H16c-8.8 0-16 7.2-16 16v16c0 35.2 28.8 64 64 64h512c35.2 0 64-28.8 64-64v-16c0-8.8-7.2-16-16-16zM576 48c0-26.4-21.6-48-48-48H112C85.6 0 64 21.6 64 48v336h512V48zm-64 272H128V64h384v256z'/></svg>"
      title: "We're in a Matrix!"
      subtitle: "See how we can exist well in it"
  b:
    - url: "/superphysics/principles/idea"
      icon: "<svg xmlns='http://www.w3.org/2000/svg' fill='coral' height='80' viewBox='0 0 352 512'><path d='M96.06 454.35c.01 6.29 1.87 12.45 5.36 17.69l17.09 25.69a31.99 31.99 0 0 0 26.64 14.28h61.71a31.99 31.99 0 0 0 26.64-14.28l17.09-25.69a31.989 31.989 0 0 0 5.36-17.69l.04-38.35H96.01l.05 38.35zM0 176c0 44.37 16.45 84.85 43.56 115.78 16.52 18.85 42.36 58.23 52.21 91.45.04.26.07.52.11.78h160.24c.04-.26.07-.51.11-.78 9.85-33.22 35.69-72.6 52.21-91.45C335.55 260.85 352 220.37 352 176 352 78.61 272.91-.3 175.45 0 73.44.31 0 82.97 0 176zm176-80c-44.11 0-80 35.89-80 80 0 8.84-7.16 16-16 16s-16-7.16-16-16c0-61.76 50.24-112 112-112 8.84 0 16 7.16 16 16s-7.16 16-16 16z'/></svg>" 
      title: "Everything is an Idea"
      subtitle: "Inside the Mind or Matrix"
  c:
    - url: "superphysics/solutions/qualimath"
      icon: "<svg xmlns='http://www.w3.org/2000/svg' fill='coral' height='80' viewBox='0 0 576 512'><path d='M571.31 251.31l-22.62-22.62c-6.25-6.25-16.38-6.25-22.63 0L480 274.75l-46.06-46.06c-6.25-6.25-16.38-6.25-22.63 0l-22.62 22.62c-6.25 6.25-6.25 16.38 0 22.63L434.75 320l-46.06 46.06c-6.25 6.25-6.25 16.38 0 22.63l22.62 22.62c6.25 6.25 16.38 6.25 22.63 0L480 365.25l46.06 46.06c6.25 6.25 16.38 6.25 22.63 0l22.62-22.62c6.25-6.25 6.25-16.38 0-22.63L525.25 320l46.06-46.06c6.25-6.25 6.25-16.38 0-22.63zM552 0H307.65c-14.54 0-27.26 9.8-30.95 23.87l-84.79 322.8-58.41-106.1A32.008 32.008 0 0 0 105.47 224H24c-13.25 0-24 10.74-24 24v48c0 13.25 10.75 24 24 24h43.62l88.88 163.73C168.99 503.5 186.3 512 204.94 512c17.27 0 44.44-9 54.28-41.48L357.03 96H552c13.25 0 24-10.75 24-24V24c0-13.26-10.75-24-24-24z'/></svg>" 
      title: "A New Math for a New Science"
      subtitle: "Has relativity baked in"

artsup:
  a:
    - url: "superphysics/what-is-superphysics"
      img: "/banner.jpg" 
      title: "What's Superphysics?"
      subtitle: "A new science based on the original Socratic Dialectics"
  b:
    - url: "superphysics/socratic-dialectics/"
      img: "/avatars/socrates.jpg" 
      title: "What's Socratic Dialectics?"
      subtitle: "A thinking paradigm that allows both Physics and Metaphysics"


---