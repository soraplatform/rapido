---
title: "Project KONEK"
date: 2022-08-24
image: /noimg.png
description: "The project comes to a close but the connections forged through the years will remain and will only get stronger."
author: Tin Gonzales
icon: /icons/tin.png
---


The project comes to a close but the connections forged through the years will remain and will only get stronger. 

Join us as we celebrate the milestones and achievements made by Project Konek after five years and three phases, and acknowledge the contribution of our partners in a Closing Ceremony on August 30, 2022 at 1:00 PM, LIVE here on PDRF Facebook Page. 

During the event, we will launch the Project Konek microsite and the project compendium that showcases the fruits of a successful public-private partnership.

Project KoNeK or Komunidad at Negosyo Tungo sa Katatagan is an initiative of the Philippine Disaster Resilience Foundation and USAID Bureau of Humanitarian Assistance that aims to build the disaster risk reduction and management (DRRM) capabilities of local communities. Notable Project KoNeK activities include capacity building of barangays through workshops on evacuation camp management, DRRM, contingency planning, and creating hazard maps.



